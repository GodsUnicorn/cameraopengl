#pragma once

#include <iostream>

#include <glew.h>
#include <glfw3.h>

#include <GLM/GLM/glm.hpp>
#include <GLM/GLM/gtc/matrix_transform.hpp>

class Camera {
private:
	glm::vec3 position; // Camera position vector
	glm::vec3 frontPos; // Camera facing vector
	glm::vec3 rightPos;
	glm::vec3 upPos; // Cross product of front & right vector. (Releative)


	glm::vec3 worldUpPos; // The up position, towards the ceiling up (Not relative)

	GLfloat yaw; // Moving relative to Y-axis left from right
	GLfloat pitch; // Moving relative to Y-axis up from down

	// Speeds
	GLfloat movementSpeed;
	GLfloat rotationSpeed;

	void Update();
public:
	Camera(glm::vec3 initialPos, glm::vec3 initialUpDir, GLfloat initialYaw, GLfloat intialPitch, GLfloat intitalMovementSpeed, GLfloat initialRoataionSpeed);
	
	void KeyControl(bool* keys, GLfloat deltaTime);
	void MouseControl(GLfloat xChange, GLfloat yChange);

	glm::mat4 CalculateViewMaxtrix();
	
	~Camera();
};

