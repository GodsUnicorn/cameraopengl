#include <iostream>
#include <cmath>
#include <glew.h>
#include <glfw3.h>

#include <GLM/GLM/glm.hpp>
#include <GLM/GLM/gtc/type_ptr.hpp>
#include <GLM/GLM/mat4x4.hpp>

#include "GLWindow.h"
#include "Mesh.h"
#include "Shader.h"
#include "Camera.h"

GLfloat deltaTime = 0.0f, lastTime = 0.0f;
const int VERTEX_COUNT = 24, INDEX_COUNT = 36;
GLfloat vertecies[VERTEX_COUNT] = {
	// front
	-1.0, -1.0,  1.0,
	1.0, -1.0,  1.0,
	1.0,  1.0,  1.0,
	-1.0,  1.0,  1.0,
	// back
	-1.0, -1.0, -1.0,
	1.0, -1.0, -1.0,
	1.0,  1.0, -1.0,
	-1.0,  1.0, -1.0,
};

GLuint indecies[INDEX_COUNT] = {
	// front
	0, 1, 2,
	2, 3, 0,
	// right
	1, 5, 6,
	6, 2, 1,
	// back
	7, 6, 5,
	5, 4, 7,
	// left
	4, 0, 3,
	3, 7, 4,
	// bottom
	4, 5, 1,
	1, 0, 4,
	// top
	3, 2, 6,
	6, 7, 3,
};

int main() {

	const char* vertexPath = "Shaders/vertexcode.vert.txt";
	const char* fragmentPath = "Shaders/fragmentcode.frag.txt";

	GLWindow window = GLWindow(1600, 1000, "Triangle");
	Mesh* mesh = new Mesh();
	Shader* shader = new Shader();
	Camera* camera = new Camera(glm::vec3(0.0f, 0.0f, 0.f), glm::vec3(0.0f, 1.0f, 0.0f), -90.0f, 0.0f, 5.0f, 1.0f);

	mesh->CreateObject(vertecies, indecies, VERTEX_COUNT, INDEX_COUNT);
	shader->CreateFromFile(vertexPath, fragmentPath);

	GLuint uniformColor = 0, uniformModel = 0, uniformProjection = 0, uniformView = 0;
	glm::mat4 projection = glm::perspective(45.0f, (GLfloat)window.GetBufferWidth() / (GLfloat)window.GetBufferHeight(), 0.1f, 100.0f);

	while (!window.WindowShouldClose()) {
		GLfloat currentTime = glfwGetTime();
		deltaTime = currentTime - lastTime;
		lastTime = currentTime;

		glfwPollEvents();

		camera->KeyControl(window.GetKeys(), deltaTime);
		camera->MouseControl(window.GetXCurrent(), window.GetYCurrent());

		float specialColor = (sin(glfwGetTime()) / 2) + 0.5f;
		float specialColor2 = (cos(glfwGetTime()) / 2) + 0.5f;

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader->UseProgram();

		uniformColor = shader->ReturnUniformColor();
		uniformModel = shader->ReturnUniformModel();
		uniformProjection = shader->ReturnUniformProjection();
		uniformView = shader->ReturnUniformView();
	
		glm::mat4 model(1.0);

		model = glm::translate(model, glm::vec3(0.0f, 0.0f, -2.5f));
		model = glm::rotate(model, (45.0f / 180.0f) * 3.1416f, glm::vec3(1.0f, 1.0f, 0.0f));
		// model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));

		glUniform4f(uniformColor, 0.2f / specialColor, specialColor2, specialColor, specialColor2);
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(camera->CalculateViewMaxtrix()));
		
		mesh->RenderObject();

		shader->UnuseProgram();

		window.SwapBuffers();
	}

	delete camera;
	delete shader;
	delete mesh;

	return 0;

}