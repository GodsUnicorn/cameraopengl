#ifndef MESH_H
#define MESH_H

#include <glew.h>

class Mesh {
private:
	GLuint vertexCount, indexCount;
	GLuint VAO, VBO, EBO;
	
	void ClearObject();
public:
	Mesh();

	template<typename T>
	void CreateObject(T* vertecies, GLuint* indecies,  const int numberVertecies, const int numberIndices);
	void RenderObject();

	~Mesh();
};


template<typename T>
void Mesh::CreateObject(T* vertecies, GLuint* indecies, const int numberVertecies, const int numberIndices) {

	vertexCount = numberVertecies;
	indexCount = numberIndices;

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertecies[0]) * numberVertecies, vertecies, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indecies[0]) * numberIndices, indecies, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
#endif // !MESH_H

