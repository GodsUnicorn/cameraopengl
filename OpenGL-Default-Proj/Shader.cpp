#include "Shader.h"

Shader::Shader() {
	program = 0; 
	color = 0, model = 0, projection = 0;
}

void Shader::CreateShader(GLuint& shader, const char* shaderCode, GLenum type) {
	shader = glCreateShader(type);	
	glShaderSource(shader, 1, &shaderCode, 0);

	glCompileShader(shader);

	int success = 0;
	GLchar log[500];

	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	if(!success) {
		glGetShaderInfoLog(shader, sizeof(log), 0, log);
		std::cout << "Error compiling shader:\n" << log << std::endl;
		return;
	}

	glAttachShader(program, shader);
}

void Shader::CompileProgram(const char* vertexCode, const char* fragmentCode) {
	program = glCreateProgram();

	if (!program) {
		std::cout << "OpenGL failed to create program." << std::endl;
		system("pause");
		return;
	}

	GLuint vertexShader = 0, fragmentShader = 0;
	CreateShader(vertexShader, vertexCode, GL_VERTEX_SHADER);
	CreateShader(fragmentShader, fragmentCode, GL_FRAGMENT_SHADER);

	glLinkProgram(program);

	int success = 0;
	GLchar log[500];

	glGetProgramiv(program, GL_LINK_STATUS, &success);

	if (!success) {
		glGetProgramInfoLog(program, sizeof(log), 0, log);
		std::cout << "Error linking program: " << log << std::endl;
		return;
	}

	glValidateProgram(program);

	glGetProgramiv(program, GL_VALIDATE_STATUS, &success);

	if (!success) {
		glGetProgramInfoLog(program, sizeof(log), 0, log);
		std::cout << "Error validating program: " << log << std::endl;
		return;
	}

	// Uniform Variables here
	projection = glGetUniformLocation(program, "uProjection");
	model = glGetUniformLocation(program, "uModel");
	color = glGetUniformLocation(program, "uColor");
	view = glGetUniformLocation(program, "uView");

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void Shader::ClearShader() {
	if (program != 0) {
		glDeleteProgram(program);
		program = 0;
	}
}

std::string Shader::ReadFile(const char* fileLocation) {
	std::ifstream shaderFile;
	std::string fileContent = "";

	shaderFile.open(fileLocation, std::ios::in);

	if (!shaderFile.is_open()) {
		std::cout << "Could not open file: " << fileLocation << std::endl;
		system("pause");
		return "";
	}

	std::string lineCode = "";

	while (!shaderFile.eof()) {
		getline(shaderFile, lineCode);
		fileContent.append(lineCode + "\n");
	}

	shaderFile.close();

	return fileContent;

}

void Shader::CreateFromString(const char* vertexCode, const char* fragmentCode) {
	CompileProgram(vertexCode, fragmentCode);
}

void Shader::CreateFromFile(const char* vertexPath, const char* fragmentPath) {
	std::string vertexString = ReadFile(vertexPath);
	std::string fragmentString = ReadFile(fragmentPath);

	const char* vertexCode = vertexString.c_str();
	const char* fragmentCode = fragmentString.c_str();

	CompileProgram(vertexCode, fragmentCode);
}

void Shader::UseProgram() {
	glUseProgram(program);
}
void Shader::UnuseProgram() {
	glUseProgram(0);
}

GLuint Shader::ReturnUniformColor() { return color; }
GLuint Shader::ReturnUniformModel() { return model; }
GLuint Shader::ReturnUniformProjection() { return projection; }
GLuint Shader::ReturnUniformView() { return view; }

Shader::~Shader() {
	ClearShader();
}
