#ifndef GLWINDOW_H
#define GLWINDOW_H

#include <iostream>
#include <glew.h>
#include <glfw3.h>

class GLWindow {
private:
	GLFWwindow* window;
	const char* name;
	GLuint width, height;
	GLint bufferWidth, bufferHeight;

	bool keys[1024]; // Range of ascii characters for keyboard input.

	// Mouse input variables
	GLfloat previousX, previousY;
	GLfloat xCurrent, yCurrent;
	bool mouseFirstMovement;

	void Initialize();
	void Clear();
public:
	GLWindow();
	
	GLWindow(GLuint windowWidth, GLuint windowHeight, const char* windowName);
	
	void SwapBuffers();
	void CreateCallbacks();

	GLint GetBufferWidth();
	GLint GetBufferHeight();

	bool WindowShouldClose();

	bool* GetKeys();

	GLfloat GetXCurrent();
	GLfloat GetYCurrent();

	// Callbacks cannot go to class members, that is why it is static.
	// Static methods belong to this class, and can be accessed without having a class object.
	static void HandleKeys(GLFWwindow* window, int key, int code, int action, int mode); // Function has to static.
	static void HandleMouse(GLFWwindow* window, double xPos, double yPos);

	~GLWindow();
};



#endif // !GLWINDOW_H