#include "Camera.h"

Camera::Camera(glm::vec3 initialPos, glm::vec3 initialUpDir, GLfloat initialYaw, GLfloat intialPitch, GLfloat intitalMovementSpeed, GLfloat initialRoataionSpeed) {
	position = initialPos;
	worldUpPos = initialUpDir;
	yaw = initialYaw;
	pitch = intialPitch;

	frontPos = glm::vec3(0.0f, 0.0f, -1.0f);

	movementSpeed = intitalMovementSpeed;
	rotationSpeed = initialRoataionSpeed;

	Update();
}

void Camera::Update() {
	frontPos.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	frontPos.y = sin(glm::radians(pitch));
	frontPos.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	frontPos = glm::normalize(frontPos);

	rightPos = glm::normalize(glm::cross(frontPos, worldUpPos));
	upPos = glm::normalize(glm::cross(rightPos, frontPos));
}

void Camera::KeyControl(bool* keys, GLfloat deltaTime) {

	GLfloat velocity = movementSpeed * deltaTime;

	if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
		position += frontPos * velocity;
	}

	if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
		position -= frontPos * velocity;
	}

	if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
		position -= rightPos * velocity;
	}

	if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
		position += rightPos * velocity;
	}

}

void Camera::MouseControl(GLfloat xChange, GLfloat yChange) {
	xChange *= rotationSpeed;
	yChange *= rotationSpeed;

	yaw += xChange;
	pitch += yChange;

	if (pitch > 89.0f) 
		pitch = 89.0f;
	

	if (pitch < -89.0f)
		pitch = -89.0f;

	Update();
}

glm::mat4 Camera::CalculateViewMaxtrix() {
	return glm::lookAt(position, position + frontPos, upPos);
}

Camera::~Camera() {
}
