#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <string>
#include <fstream>

#include <glew.h>

class Shader {
private:
	GLuint program;
	GLuint color, model, projection, view;
	void CreateShader(GLuint& shader, const char* shaderCode, GLenum type);
	void CompileProgram(const char* vertexCode, const char* fragmentCode);
	void ClearShader();

	std::string ReadFile(const char* fileLocation);
public:
	Shader();
	
	void CreateFromString(const char* vertexCode, const char* fragmentCode);
	void CreateFromFile(const char* vertexPath, const char* fragmentPath);

	void UseProgram();
	void UnuseProgram();

	GLuint ReturnUniformColor();
	GLuint ReturnUniformModel();
	GLuint ReturnUniformProjection();
	GLuint ReturnUniformView();

	~Shader();
};



#endif // !SHADER_H

