#include "GLWindow.h"

GLWindow::GLWindow() {
	window = 0;
	name = "Window";
	width = 800, height = 600;
	bufferWidth = 0, bufferHeight = 0;
	
	for (size_t index = 0; index < 1024; index++) {
		keys[index] = 0;
	}

	Initialize();
}

GLWindow::GLWindow(GLuint windowWidth, GLuint windowHeight, const char* windowName) {
	window = 0;
	name = windowName;
	width = windowWidth, height = windowHeight;
	bufferWidth = 0, bufferHeight = 0;

	for (size_t index = 0; index < 1024; index++) {
		keys[index] = 0;
	}

	Initialize();
}

void GLWindow::Initialize() {
	if (!glfwInit()) {
		std::cout << "Failed to initialize glfw." << std::endl;
		system("pause");
		return;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	 window = glfwCreateWindow(width, height, name, 0, 0);

	if (!window) {
		std::cout << "GLFW failed to create widow." << std::endl;
		system("pause");
		glfwTerminate();
		return;
	}

	glfwMaximizeWindow(window);

	glfwGetFramebufferSize(window, &bufferWidth, &bufferHeight);
	glfwMakeContextCurrent(window);


	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK) {
		std::cout << "Failed to initialize GLEW." << std::endl;
		system("pause");
		glfwTerminate();
		return;
	}

	// Key and mouse inputs
	CreateCallbacks();

	glEnable(GL_DEPTH_TEST);

	glViewport(0, 0, bufferWidth, bufferHeight);

	glfwSetWindowUserPointer(window, this); // setting this class is the user of the window.	
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // Locks cursor inside window.
}

void GLWindow::Clear() {
	glfwTerminate();
}

void GLWindow::SwapBuffers() { glfwSwapBuffers(window); }

GLint GLWindow::GetBufferWidth() { return bufferWidth; }
GLint GLWindow::GetBufferHeight() { return bufferHeight; }

bool GLWindow::WindowShouldClose() { return glfwWindowShouldClose(window); }

bool* GLWindow::GetKeys() { return keys; }

GLfloat GLWindow::GetXCurrent() {
	GLfloat theChange = xCurrent;
	xCurrent = 0;
	return theChange;
}
GLfloat GLWindow::GetYCurrent() {
	GLfloat theChange = yCurrent;
	yCurrent = 0;
	return theChange;
}

void GLWindow::CreateCallbacks() {
	glfwSetKeyCallback(window, HandleKeys);
	glfwSetCursorPosCallback(window, HandleMouse);
}

// GLFW knows the parameters Ex: key, code, action, mode, xPos, yPos
void GLWindow::HandleKeys(GLFWwindow* window, int key, int code, int action, int mode) {
	GLWindow* thisWindow = static_cast<GLWindow*>(glfwGetWindowUserPointer(window)); // Casting the window to this definition of window, so this new window equals our window in source.cpp

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key >= 0 && key < 1024) {

		if (action == GLFW_PRESS) 
			thisWindow->keys[key] = true;
		 else if(action == GLFW_RELEASE) 
			thisWindow->keys[key] = false;

	}
}
void GLWindow::HandleMouse(GLFWwindow* window, double xPos, double yPos) {
	GLWindow* thisWindow = static_cast<GLWindow*>(glfwGetWindowUserPointer(window));

	if (thisWindow->mouseFirstMovement) {
		// Starting positions, will always 0 on start
		// Our previous position will equal the value that GLFW calculated.
		thisWindow->previousX = xPos;  
		thisWindow->previousY = yPos;

		thisWindow->mouseFirstMovement = false; // For initialization of the previous variables.
	}

	// The current position equals the position of cursor - previousX
	// Isn't previousX equal to xPos????
	thisWindow->xCurrent = xPos - thisWindow->previousX; // If mouse move by -2, 
	thisWindow->yCurrent = thisWindow->previousY - yPos;

	// std::cout << "X pos: " << xPos << ", x current: " << thisWindow->xCurrent << std::endl;


	thisWindow->previousX = xPos;
	thisWindow->previousY = yPos;

	// std::cout << "x: " << thisWindow->xCurrent << ", y: " << thisWindow->yCurrent << std::endl;
}

GLWindow::~GLWindow() {
	Clear();
}
